### STAGE 1: Build ###
FROM node AS build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
COPY . /app
RUN npm install
# RUN ng build --prod
RUN ng serve --prod --port 80
# ### STAGE 2: Run ###
# FROM nginx
# RUN ls /usr/share/nginx/html
# COPY --from=build /app/dist/skinlot /usr/share/nginx/html
# COPY ./default.conf /etc/nginx/conf.d/default.conf
# RUN ls /usr/share/nginx/html
EXPOSE 80
# run nginx
# CMD ["nginx", "-g", "daemon off;"]
